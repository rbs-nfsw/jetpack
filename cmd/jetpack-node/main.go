package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/dghubble/sling"
	"github.com/labstack/echo"
	"gitlab.com/rbs-nfsw/jetpack/server"
)

var (
	motd         string
	host         string
	secure       bool
	presenceKey  string
	listenAddr   string
	masterServer string
)

func init() {
	flag.StringVar(&motd, "motd", "", "Server MOTD")
	flag.StringVar(&host, "host", "", "Server Host")
	flag.BoolVar(&secure, "secure", false, "Server gets queried over HTTPS")
	flag.StringVar(&presenceKey, "presenceKey", "nfsw", "Discord Rich Presence image name")
	flag.StringVar(&listenAddr, "listen", "", "Node listen address")
	flag.StringVar(&masterServer, "masterserver", "https://jetpack.nextdata.eu", "Masterserver URL")
}

func main() {
	flag.Parse()
	if motd == "" || host == "" || listenAddr == "" {
		flag.PrintDefaults()
		return
	}

	n := NewNode()
	go n.RunCheckServer(listenAddr)
	go n.RunAnnouncer()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig

	logrus.Info("Shutting down...")
	n.Shutdown()
}

func retry(t time.Duration, f func() bool, cancel chan struct{}) {
	for {
		if f() {
			break
		}
		select {
		case <-time.After(t):
		case <-cancel:
			return
		}
	}
}

func NewNode() *Node {
	return &Node{
		checkSecret: server.GenerateRandomString(32),
		stop:        make(chan struct{}),
	}
}

type Node struct {
	checkSecret string
	stop        chan struct{}
	wg          sync.WaitGroup
	challenge   string
}

func (n *Node) Shutdown() {
	close(n.stop)
	n.wg.Wait()

	url := fmt.Sprintf("%v/servers/shutdown", masterServer)
	req, _ := sling.New().Post(url).BodyJSON(server.ShutdownRequest{
		Challenge: n.challenge,
	}).Request()
	http.DefaultClient.Do(req)

	// it was not my idea
	panic("self destruct")
}

func (n *Node) RunAnnouncer() {
	n.wg.Add(1)
	defer n.wg.Done()

	for {
		requ := server.ServerAddRequest{
			Challenge:          n.challenge,
			CheckSecret:        n.checkSecret,
			MOTD:               motd,
			Host:               host,
			Secure:             secure,
			DiscordPresenceKey: presenceKey,
		}
		var resp server.ServerAddResponse
		for {
			failed := func() bool {
				url := fmt.Sprintf("%v/servers/add", masterServer)
				res, err := sling.New().Post(url).BodyJSON(requ).ReceiveSuccess(&resp)
				if err != nil {
					logrus.WithError(err).Error("Failed to send server add request; retrying in 30 seconds")
					return true
				}
				if res.StatusCode != http.StatusOK {
					logrus.WithField("status", res.StatusCode).Error("Server responded with error; retrying in 30 seconds")
					return true
				}
				return false
			}()
			if !failed {
				break
			}
			select {
			case <-time.After(30 * time.Second):
			case <-n.stop:
				return
			}
		}
		n.challenge = resp.Challenge
		logrus.WithField("challenge", n.challenge).Info("Server added successfully!")
		select {
		case <-time.After(5 * time.Minute):
		case <-n.stop:
			return
		}
	}
}

func (n *Node) RunCheckServer(addr string) {
	n.wg.Add(1)
	defer n.wg.Done()

	e := echo.New()
	e.HideBanner = true
	e.HidePort = true

	e.GET("/serverlist-node/check", func(c echo.Context) error {
		hm := hmac.New(sha256.New, []byte(n.checkSecret))
		hm.Write([]byte(c.QueryParam("nonce")))
		return c.String(http.StatusOK, hex.EncodeToString(hm.Sum(nil)))
	})

	go func() {
		<-n.stop
		e.Close()
	}()

	e.Start(addr)
}
