package main

import "gitlab.com/rbs-nfsw/jetpack/server"

func main() {
	s := server.NewServer()
	s.Run(":8088")
}
