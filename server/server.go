package server

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/patrickmn/go-cache"
)

func NewServer() *Server {
	return &Server{
		serverList: cache.New(10*time.Minute, 30*time.Second),
	}
}

type Server struct {
	serverList *cache.Cache
}

func (s *Server) Run(addr string) {
	e := echo.New()
	e.Use(middleware.Logger())

	e.POST("/servers/add", func(c echo.Context) error {
		var request ServerAddRequest
		err := c.Bind(&request)
		if err != nil {
			return err
		}

		challenge := request.Challenge
		_, challengeOk := s.serverList.Get(request.Challenge)
		if !challengeOk {
			challenge = GenerateRandomString(32)
		}

		request.Challenge = challenge

		nonce := GenerateRandomString(32)
		checkUrl := url.URL{
			Scheme: request.URLScheme(),
			Host:   request.Host,
			Path:   "/serverlist-node/check",
			RawQuery: url.Values{
				"nonce": []string{nonce},
			}.Encode(),
		}
		res, err := http.Get(checkUrl.String())
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "check failure")
		}
		b, _ := ioutil.ReadAll(res.Body)
		if ValidateCheckResult(nonce, request.CheckSecret, string(b)) != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "check failure")
		}

		s.serverList.Set(challenge, request.AsServerlistEntry(), cache.DefaultExpiration)

		return c.JSON(http.StatusOK, ServerAddResponse{
			Challenge: challenge,
		})
	})

	e.POST("/servers/shutdown", func(c echo.Context) error {
		var request ShutdownRequest
		if err := c.Bind(&request); err != nil {
			return err
		}

		s.serverList.Delete(request.Challenge)
		c.Response().WriteHeader(http.StatusOK)
		return nil
	})

	e.GET("/servers", func(c echo.Context) error {
		var query ServerlistQuery
		if err := c.Bind(&query); err != nil {
			return err
		}
		list := make([]ServerlistEntry, s.serverList.ItemCount())
		i := 0
		for _, it := range s.serverList.Items() {
			list[i] = it.Object.(ServerlistEntry)
			i++
		}
		if query.Limit != nil {
			list = list[:min(len(list), *query.Limit)]
		}
		if query.Pretty {
			return c.JSONPretty(http.StatusOK, list, "    ")
		}
		return c.JSON(http.StatusOK, list)
	})

	e.GET("/servers/legacy", func(c echo.Context) error {
		var query ServerlistQuery
		if err := c.Bind(&query); err != nil {
			return err
		}
		list := make([]ServerlistEntry, s.serverList.ItemCount())
		i := 0
		for _, it := range s.serverList.Items() {
			list[i] = it.Object.(ServerlistEntry)
			i++
		}
		if query.Limit != nil {
			list = list[:min(len(list), *query.Limit)]
		}
		var out bytes.Buffer
		for _, srv := range list {
			out.WriteString(srv.MOTD)
			out.WriteString(";")
			out.WriteString(srv.Address)
			if !query.SkipDiscord {
				out.WriteString(";")
				out.WriteString(srv.DiscordPresenceKey)
			}
			out.WriteString("\n")
		}
		return c.String(http.StatusOK, out.String())
	})

	e.Start(addr)
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}
