package server

import (
	"crypto/rand"
	"math/big"
)

const randomCharset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func GenerateRandomString(length int) string {
	out := make([]byte, length)
	for i := 0; i < length; i++ {
		bigIndex, _ := rand.Int(rand.Reader, big.NewInt(int64(len(randomCharset))))
		index := int(bigIndex.Int64())
		out[i] = randomCharset[index]
	}
	return string(out)
}
