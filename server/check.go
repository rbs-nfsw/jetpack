package server

import (
	"crypto/hmac"
	"encoding/hex"
	"errors"

	"gitlab.com/rbs-nfsw/jetpack"
)

var CheckValidationError = errors.New("check result is incorrect")

func ValidateCheckResult(nonce string, secret string, result string) error {
	correctRes := jetpack.CalculateCheckResult(nonce, secret)
	resultB, err := hex.DecodeString(result)
	if err != nil {
		return err
	}
	if !hmac.Equal(correctRes, resultB) {
		return CheckValidationError
	}
	return nil
}
