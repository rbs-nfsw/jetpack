package server

import (
	"path"
	"strings"
)

func CheckModFilename(filename string) bool {
	if strings.ContainsAny(filename, "\\:?*") {
		return false
	}

	if strings.HasPrefix(filename, "/") {
		return false
	}

	if strings.Contains(filename, "..") {
		return false
	}

	if !checkModExtension(path.Ext(filename)) {
		return false
	}

	allowedSubdirs := []string{
		"CARS",
		"FRONTEND",
		"GFX",
		"GLOBAL",
		"LANGUAGES",
		"Media",
		"Movies",
		"Sound",
		"Tracks",
		"TracksHigh",
	}
	pathSplits := strings.Split(filename, "/")
	if len(pathSplits) == 1 {
		return false
	}

	for _, subDir := range allowedSubdirs {
		if strings.EqualFold(subDir, pathSplits[0]) {
			return true
		}
	}

	return false
}

func checkModExtension(ext string) bool {
	deniedExts := []string{".exe", ".dll", ".asi"}
	for _, dExt := range deniedExts {
		if dExt == ext {
			return false
		}
	}
	return true
}
