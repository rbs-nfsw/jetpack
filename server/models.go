package server

import (
	"net/url"
)

type ServerlistEntry struct {
	MOTD               string `json:"motd"`
	DiscordPresenceKey string `json:"discordPresenceKey"`
	Address            string `json:"address"`
}

type ServerAddRequest struct {
	Challenge          string `json:"challenge,omitempty"`
	CheckSecret        string `json:"checkSecret"`
	MOTD               string `json:"motd"`
	DiscordPresenceKey string `json:"discordPresenceKey"`
	Host               string `json:"host"`
	Secure             bool   `json:"secure"`
}

func (r ServerAddRequest) URLScheme() string {
	if r.Secure {
		return "https"
	} else {
		return "http"
	}
}

func (r ServerAddRequest) AsServerlistEntry() ServerlistEntry {
	url := &url.URL{
		Scheme: r.URLScheme(),
		Host:   r.Host,
		Path:   "/soapbox-race-core/Engine.svc",
	}
	return ServerlistEntry{
		MOTD:               r.MOTD,
		DiscordPresenceKey: r.DiscordPresenceKey,
		Address:            url.String(),
	}
}

type ServerAddResponse struct {
	Challenge string `json:"challenge"`
}

type ServerlistQuery struct {
	SkipDiscord bool `query:"skipDiscord"`
	Limit       *int `query:"limit"`
	Pretty      bool `query:"pretty"`
}

type ShutdownRequest struct {
	Challenge string `json:"challenge"`
}
