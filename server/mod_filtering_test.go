package server

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestModFiltering(t *testing.T) {
	assert.False(t, CheckModFilename("/test.txt"))
	assert.False(t, CheckModFilename("C:\\test.txt"))
	assert.False(t, CheckModFilename("C:/test.txt"))
	assert.False(t, CheckModFilename("../test.txt"))
	assert.False(t, CheckModFilename("GLOBAL/test.asi"))
	assert.False(t, CheckModFilename("GLOBAL/test.exe"))
	assert.False(t, CheckModFilename("GLOBAL/test.dll"))
	assert.False(t, CheckModFilename("test/Test.gfx"))

	assert.True(t, CheckModFilename("GFX/Test.gfx"))
}
