package jetpack

import (
	"crypto/hmac"
	"crypto/sha256"
)

func CalculateCheckResult(nonce string, secret string) []byte {
	mac := hmac.New(sha256.New, []byte(secret))
	mac.Write([]byte(nonce))
	return mac.Sum(nil)
}
