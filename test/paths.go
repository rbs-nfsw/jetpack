package main

import (
	"fmt"
	"path/filepath"
)

func main() {
	fmt.Println(filepath.IsAbs("C:\\"), filepath.IsAbs("C:/"))
}
